from datetime import date

def main():
    current_day = date.today().day
    print(f"Today is the {current_day}")

if __name__ == "__main__":
    main()