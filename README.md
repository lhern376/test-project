# ITDP Project (Dell's Hackaton)

### Table of Contents
1. [Overview](#Overview)
1. [Product Spec](#Product-Spec)
1. [Wireframes](#Wireframes)
2. [Schema](#Schema)

## Overview
### Description



## Product Spec
### User Stories (Required and Optional)

### 1. Participant
**Required Stories**


**Optional Nice-to-have Stories**


### 1. Manager and Lead
**Required Stories**


**Optional Nice-to-have Stories**



## Wireframes



## Schema 
### Models



**Model: User**



**Model: Event**



**Model: Registration**



### Model Implementation
